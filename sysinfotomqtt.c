/*
 * Copyright 2018 Kurt Van Dijck <dev.kurt@vandijck-laurijssen.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * Some parts are copied from slstatus (https://git.suckless.org/slstatus)
 * which falls under:
 *
 * ISC License
 *
 * Copyright 2016-2017 Aaron Marcher <me@drkhsh.at>
 *
 * Copyright 2016 Roy Freytag <rfreytag@hs-mittweida.de>
 * Copyright 2016 Vincent Loupmon <vincentloupmon@gmail.com>
 * Copyright 2016 Daniel Walter <d.walter@0x90.at>
 * Copyright 2016 Jody Leonard <me@jodyleonard.com>
 * Copyright 2016 Mike Coddington <mike@coddington.us>
 * Copyright 2016-2017 Ali H. Fardan <raiz@firemail.cc>
 * Copyright 2016 parazyd <parazyd@dyne.org>
 * Copyright 2016-2017 Quentin Rameau <quinq@fifth.space>
 * Copyright 2017 Tobias Stoeckmann <tobias@stoeckmann.org>
 * Copyright 2017 Laslo Hunhold <dev@frign.de>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <getopt.h>
#include <glob.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <locale.h>
#include <poll.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <mosquitto.h>

#include "libet/libt.h"
#include "common.h"

#define NAME "sysinfotomqtt"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

#define ESTR(num)	strerror(num)

/* program options */
static const char help_msg[] =
	NAME ": Emit system info to MQTT\n"
	"usage:	" NAME " [OPTIONS ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	"\n"
	" -h, --host=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -a, --all		Emit link-local and lo addresses too\n"
	" -t, --topic		Base topic to publish under (default 'sys')\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "all", no_argument, NULL, 'a', },
	{ "topic", required_argument, NULL, 't', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:at:";

static int emitall;
static char *topicbase = "sys";

/* signal handler */
static volatile int sigterm;
static volatile int ready;

/* logging */
static int loglevel = LOG_WARNING;

/* MQTT parameters */
static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 10;
static int mqtt_qos = -1;

/* state */
static struct mosquitto *mosq;

/* signalling */
static int mysignal(int signr, void (*fn)(int))
{
	struct sigaction sa = {
		.sa_handler = fn,
	};
	return sigaction(signr, &sa, NULL);
}
static void onsigterm(int signr)
{
	sigterm = 1;
}

static void myfree(void *dat)
{
	if (dat)
		free(dat);
}

/* ADDR */
struct iface {
	char name[IFNAMSIZ+1];
	char *prevvalue;
	char *value;
	int nvalue, svalue;
};

static struct iface *ifaces;
static int nifaces, sifaces;

static struct iface *add_iface(const char *name)
{
	struct iface *iface;

	if (nifaces+1 > sifaces) {
		sifaces += 16;
		ifaces = realloc(ifaces, sizeof(*ifaces)*sifaces);
		if (!ifaces)
			mylog(LOG_ERR, "realloc %i ifaces: %s", sifaces, ESTR(errno));
	}
	iface = &ifaces[nifaces++];
	memset(iface, 0, sizeof(*iface));

	strncpy(iface->name, name, sizeof(iface->name)-1);
	return iface;
}

static void remove_iface(struct iface *iface)
{
	int idx = iface - ifaces;

	myfree(iface->value);
	myfree(iface->prevvalue);
	if (idx != nifaces-1) {
		memcpy(ifaces+idx, ifaces+nifaces-1, sizeof(*ifaces));
	}
	--nifaces;
}

static int ifacecmp(const void *a, const void *b)
{
	const struct iface *ia = a, *ib = b;

	return strcmp(ia->name, ib->name);
}

static inline void sort_ifaces(void)
{
	qsort(ifaces, nifaces, sizeof(*ifaces), ifacecmp);
}

static struct iface *find_iface_by_name(const char *iface, int create)
{
	struct iface *result;
	struct iface needle = {};

	strncpy(needle.name, iface, sizeof(needle.name)-1);

	result = bsearch(&needle, ifaces, nifaces, sizeof(*ifaces), ifacecmp);
	if (!result && create) {
		add_iface(iface);
		sort_ifaces();
		/* search again, should produce a result */
		result = find_iface_by_name(iface, 0);
	}
	return result;
}

/* MQTT */
static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	if (is_self_sync(msg))
		ready = 1;
}

__attribute__((format(printf,3,4)))
static void publish_value(int retained, const char *value, const char *topicfmt, ...)
{
	va_list va;
	int ret;
	static char topic[1024];

	va_start(va, topicfmt);
	vsprintf(topic, topicfmt, va);
	va_end(va);

	/* publish cache */
	ret = mosquitto_publish(mosq, NULL, topic, strlen(value ?: ""), value, mqtt_qos, retained);
	if (ret)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", topic, mosquitto_strerror(ret));
}

__attribute__((format(printf,1,2)))
static const char *fmtval(const char *fmt, ...)
{
	va_list va;
	static char value[1024];

	va_start(va, fmt);
	vsprintf(value, fmt, va);
	va_end(va);

	return value;
}

static const char *addrtostr(const struct sockaddr *addr)
{
	static char buf[1024];

	switch (addr->sa_family) {
	case AF_INET:
		if (!inet_ntop(addr->sa_family, &((struct sockaddr_in *)addr)->sin_addr,
				buf, sizeof(buf)-1))
			return NULL;
		if (!emitall && !strncmp(buf, "169.254.", 8))
			return NULL;
		break;
	case AF_INET6:
		if (!inet_ntop(addr->sa_family, &((struct sockaddr_in6 *)addr)->sin6_addr,
				buf, sizeof(buf)-1))
			return NULL;
		if (!emitall && !strncmp(buf, "fe", 2))
			return NULL;
		break;
	default:
		return NULL;
	}
	return buf;
}

static void publish_addrs(void *dat)
{
	struct iface *iface;

	/* reset composer */
	for (iface = ifaces; iface < ifaces+nifaces; ++iface) {
		if (iface->nvalue)
			iface->value[0] = 0;
		iface->nvalue = 0;
	}
	/* compose */
	struct ifaddrs *table = NULL, *ptr;
	int len;
	const char *value;

	/* fetch data */
	if (getifaddrs(&table) < 0)
		mylog(LOG_ERR, "getifaddrs failed: %s", ESTR(errno));

	/* print results */
	for (ptr = table; ptr; ptr = ptr->ifa_next) {
		if (!ptr->ifa_addr)
			continue;
		if (!emitall && !strcmp(ptr->ifa_name, "lo"))
			continue;
		value = addrtostr(ptr->ifa_addr);
		if (!value)
			continue;
		len = strlen(value);
		/* append value to iface */
		iface = find_iface_by_name(ptr->ifa_name, 1);
		if (iface->nvalue + 1+len+1 > iface->svalue) {
			iface->svalue += len+2;
			/* align size */
			iface->svalue = (iface->svalue +63) & ~63;
			iface->value = realloc(iface->value, iface->svalue);
			if (!iface->value)
				mylog(LOG_ERR, "realloc %u: %s", iface->svalue, ESTR(errno));
		}
		/* insert seperator */
		if (iface->nvalue)
			iface->value[iface->nvalue++] = ' ';
		strcpy(iface->value+iface->nvalue, value);
		iface->nvalue += len;
	}
	/* dispose */
	freeifaddrs(table);

	/* publish */
	int need_sort = 0;
	for (iface = ifaces; iface < ifaces+nifaces; ++iface) {
		if (!strcmp(iface->prevvalue ?: "", iface->value ?: ""))
			continue;

		publish_value(1, iface->value, "net/%s/addr", iface->name);
		if (!iface->value || !iface->value[0]) {
			remove_iface(iface);
			--iface;
			need_sort = 1;
		} else {
			myfree(iface->prevvalue);
			iface->prevvalue = strdup(iface->value ?: "");
		}
	}
	if (need_sort)
		sort_ifaces();
}

#ifndef GLOB_TILDE
#warning Your glob does not support GLOB_TILDE
#define GLOB_TILDE 0
#endif
/* from slstatus: */
static int pscanf(const char *path, const char *fmt, ...)
{
	static FILE *fp;
	va_list ap;
	int n;

	if (path) {
		char *xpath;

		if (fp)
			fclose(fp);
		if (strpbrk(path, "*~?")) {
			glob_t glb;

			if (glob(path, GLOB_TILDE, NULL, &glb) < 0)
				return -1;
			xpath = strdup(glb.gl_pathv[0]);
			globfree(&glb);
		} else
			xpath = (char *)path;
		fp = fopen(xpath, "r");
		if (!fp)
			mylog(LOG_ERR, "fopen %s: %s\n", xpath, strerror(errno));
		if (path != xpath)
			free(xpath);
		if (!fp)
			return -1;
	}
	va_start(ap, fmt);
	n = vfscanf(fp, fmt, ap);
	va_end(ap);

	return (n == EOF) ? -1 : n;
}

/* scan is inspired by slstatus: */
static void publish_cpu(void *dat)
{
	static long double a[7];
	static int valid;
	long double b[7];
	double cpu = NAN, iowait = NAN;

	memcpy(b, a, sizeof(b));
	if (pscanf("/proc/stat", "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf", &a[0], &a[1], &a[2],
	           &a[3], &a[4], &a[5], &a[6]) != 7)
		mylog(LOG_ERR, "scan /proc/stat didn't produce 7 elements?");
	if (!valid) {
		valid = 1;
		goto unknown;
	}
	cpu = ((b[0]+b[1]+b[2]+b[5]+b[6]) - (a[0]+a[1]+a[2]+a[5]+a[6])) /
	       ((b[0]+b[1]+b[2]+b[3]+b[4]+b[5]+b[6]) - (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));
	iowait = ((b[4]) - (a[4])) /
	       ((b[0]+b[1]+b[2]+b[3]+b[4]+b[5]+b[6]) - (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]));
unknown:
	publish_value(0, fmtval("%.3lf", cpu), "%s/cpu", topicbase);
	publish_value(0, fmtval("%.3lf", iowait), "%s/iowait", topicbase);
}

static void publish_uptime(void *dat)
{
	long double t;

	if (pscanf("/proc/uptime", "%Lf", &t) != 1)
		mylog(LOG_ERR, "scan /proc/uptime didn't produce 1 element?");
	publish_value(0, fmtval("%.0Lf", t), "%s/uptime", topicbase);
}

static void publish_mem(void *dat)
{
	unsigned long long total = 0, tmp;
	int ret;
	char name[32];

	for (ret = pscanf("/proc/meminfo", "%s %llu kB\n", name, &tmp);
			ret == 2; ret = pscanf(NULL, "%s %llu kB\n", name, &tmp)) {
		if (!strcmp(name, "MemTotal:"))
			total = tmp;
		else if (!strcmp(name, "MemFree:"))
			total -= tmp;
		else if (!strcmp(name, "Buffers:"))
			total -= tmp;
		else if (!strcmp(name, "Cached:"))
			total -= tmp;
		else if (!strcmp(name, "Slab:"))
			total -= tmp;
	}

	publish_value(0, fmtval("%lluM", total / 1024), "%s/mem", topicbase);
}

static void publish_info(void *dat)
{
	publish_addrs(dat);
	publish_cpu(dat);
	publish_uptime(dat);
	publish_mem(dat);
	libt_add_timeout(1, publish_info, dat);
}

int main(int argc, char *argv[])
{
	int opt, ret, j;
	char *str;
	char mqtt_name[32];
	struct pollfd pf[2];

	setlocale(LC_ALL, "");
	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;
	case 'h':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'a':
		emitall = 1;
		break;
	case 't':
		topicbase = optarg;
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	setmylog(NAME, 0, LOG_LOCAL2, loglevel);
	mysignal(SIGINT, onsigterm);
	mysignal(SIGTERM, onsigterm);

	/* MQTT start */
	if (mqtt_qos < 0)
		mqtt_qos = !strcmp(mqtt_host ?: "", "localhost") ? 0 : 1;
	mosquitto_lib_init();
	sprintf(mqtt_name, "%s-%i", NAME, getpid());
	mosq = mosquitto_new(mqtt_name, true, NULL);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));
	/* mosquitto_will_set(mosq, "TOPIC", 0, NULL, mqtt_qos, 1); */

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));
	mosquitto_message_callback_set(mosq, my_mqtt_msg);

	/* prepare poll */
	pf[0].fd = mosquitto_socket(mosq);
	pf[0].events = POLL_IN;
	/* fire first publish */
	publish_info(NULL);

	while (!sigterm) {
		libt_flush();
		if (mosquitto_want_write(mosq)) {
			ret = mosquitto_loop_write(mosq, 1);
			if (ret) {
				mylog(LOG_WARNING, "mosquitto_loop_write: %s", mosquitto_strerror(ret));
			}
		}
		ret = poll(pf, 1, libt_get_waittime());
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			mylog(LOG_ERR, "poll ...");
		if (pf[0].revents) {
			/* mqtt read ... */
			ret = mosquitto_loop_read(mosq, 1);
			if (ret) {
				mylog(LOG_WARNING, "mosquitto_loop_read: %s", mosquitto_strerror(ret));
				if (ret == MOSQ_ERR_CONN_LOST)
					/* no use to try to publish more */
					exit(1);
				break;
			}
		}
		/* mosquitto things to do each iteration */
		ret = mosquitto_loop_misc(mosq);
		if (ret) {
			mylog(LOG_WARNING, "mosquitto_loop_misc: %s", mosquitto_strerror(ret));
			break;
		}
	}

	/* clean scan results in mqtt */
	for (j = 0; j < nifaces; ++j)
		publish_value(1, "", "net/%s/addr", ifaces[j].name);

	publish_value(1, "", "%s/cpu", topicbase);
	publish_value(1, "", "%s/iowait", topicbase);
	publish_value(1, "", "%s/uptime", topicbase);
	publish_value(1, "", "%s/mem", topicbase);

	/* terminate */
	send_self_sync(mosq, mqtt_qos);
	while (!ready) {
		ret = mosquitto_loop(mosq, 10, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop: %s", mosquitto_strerror(ret));
	}

#if 0
	/* free memory */
	while (nifaces)
		remove_iface(ifaces+nifaces-1);
	myfree(ifaces);

	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
#endif
	return !sigterm;
}
